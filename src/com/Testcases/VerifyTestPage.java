package com.Testcases;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import com.Test.pages.TestPage;

public class VerifyTestPage 
{
	@Test
	public VerifyTestPage()
	{

	System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
	FirefoxDriver driver = new FirefoxDriver();
	driver.manage().window().maximize();
	driver.get("http://sydneytesters.herokuapp.com/");
	TestPage tp = new TestPage(driver);
	tp.clickongetcarquote();
	tp.selectMakedropdown();
	tp.enterYear();
	tp.enterage();
	tp.enterstate();
	tp.enterEmail();
	tp.clickgetquote();
	tp.verifyvalues();
	
	driver.quit();
	
	}
	
}
