package com.Test.pages;


import org.testng.AssertJUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class TestPage 
{

	
	WebDriver driver;
	By getCarQuote = By.id("getcarquote");
	By make = By.xpath(".//*[@id='make']");
	By email = By.id("email");
	By Year =By.xpath(".//*[@id='year']");
	By driver_age = By.xpath(".//*[@id='age']");
	By state = By.xpath(".//*[@id='state']");
	By Email = By.xpath(".//*[@id='email']");
	By getquote = By.id("getquote");
	
	
	public TestPage(WebDriver driver)
	{
	 this.driver = driver;
	
	}
	
	public void clickongetcarquote()
	{
		driver.findElement(getCarQuote).click();
		
	}
	
	
	public void selectMakedropdown()
	
	{
		WebDriverWait wait = new WebDriverWait(driver, 60);// 1 minute 
		wait.until(ExpectedConditions.visibilityOfElementLocated(make));
			
		WebElement identifier = driver.findElement(By.xpath("id('make')"));
	Select select = new Select(identifier);
	select.selectByVisibleText("Lexus");

		
	}
	
	public void enterYear()
	
	{
		
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(Year));
		driver.findElement(Year).click();
		driver.findElement(Year).sendKeys("2015");
	}
	
	public void enterage()
	
	{
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(driver_age));
		driver.findElement(driver_age).click();
		driver.findElement(driver_age).sendKeys("30");
		
	}
	
	public void enterstate()
	
	{
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(state));
		
		
		WebElement identifier = driver.findElement(By.xpath(".//*[@id='state']"));
		Select select = new Select(identifier);
		select.selectByVisibleText("Victoria");
	}
    public void enterEmail()
    {
    	WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(email));
		driver.findElement(email).click();
    
         driver.findElement(email).sendKeys("Test@test.com");
    }
    
    public void clickgetquote()
    {
    	WebDriverWait wait = new WebDriverWait(driver, 60);
    	wait.until(ExpectedConditions.elementToBeClickable(getquote));
		
    	driver.findElement(getquote).click();
    }
    
    public void verifyvalues()
    {
    
    	AssertJUnit.assertTrue(driver.getPageSource().contains("30"));
    	System.out.println("Age 30 found in page source");
    	
    	AssertJUnit.assertTrue(driver.getPageSource().contains("Lexus"));
    	System.out.println("Lexus found in page source");
    	    	
    	AssertJUnit.assertTrue(driver.getPageSource().contains("Test@test.com"));
    	System.out.println("Given Email found in page source");
    	    	   	
    	AssertJUnit.assertTrue(driver.getPageSource().contains("2015"));
    	System.out.println("Year 2015 found in page source");
    	
    	AssertJUnit.assertTrue(driver.getPageSource().contains("Victoria"));
    	System.out.println("Victoria state found in page source");
    	
    	AssertJUnit.assertTrue(driver.getPageSource().contains("Monthly Premium"));
    	System.out.println("Monthly Premium state found in page source");
    
     	
    	
    }
    }


